<?php

namespace KDA\SEO\Admin;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasTranslations;
    use \KDA\Laravel\Traits\HasDynamicSidebar;

    protected $packageName='kda-seo-admin';
    protected $viewNamespace = 'kda-seo-admin';
    protected $publishViewsTo = 'vendor/kda/backpack/seo';
    

    protected $sidebars = [
        [
            'route'=>'seo',
            'label'=> 'SEO',
            'icon'=>'la-google'
        ]
    ];

    protected $routes = [
        'backpack/seo.php'
    ];

    protected $configs = [
        'kda/seoadmin.php'=> 'kda.seoadmin'
    ];
    
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
