<?php

namespace KDA\SEO\Admin\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class NavigationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SeoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\KDA\SEO\Admin\Models\SeoRecord::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/seo');
        CRUD::setEntityNameStrings('seo', 'Enregistrements SEO');
        $this->loadPermissions('seo');
    }


    
    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        
        kda_help('Les éléments marqués comme validés ne seront pas effacés lors de la regénération potentielle des enregistrements');
        $this->crud->addFilter(
            [
                'type'  => 'simple',
                'name'  => 'unvalidated',
                'label' => 'Non validés'
            ],
            false,
            function () { // if the filter is active
                $this->crud->addClause('notValidated');
            }
        );
        CRUD::addColumn([
            'name' => 'indexable',
            'attribute' => 'indexable',
            'label'=> 'Elément',
            'wrapper'   => [
                'element' => function ($crud, $column, $entry, $related_key) {
                    return $this->retrieveIndexableElement($crud, $column, $entry, $related_key);
                },
                'href' => function ($crud, $column, $entry, $related_key) {
                    return $this->retrieveIndexableHref($crud, $column, $entry, $related_key);
                },
                'target' => '_blank',
                'class' => function ($crud, $column, $entry, $related_key) {
                    return $this->retrieveRelatedClass($crud, $column, $entry, $related_key);
                },
            ],
           // 'searchLogic' => function ($query, $column, $searchTerm) {
                //$query->orWhere('title', 'like', '%'.$searchTerm.'%');
              /*  $query->orWhereHasMorph('indexed','*',function($query){
                    $query->where('title')
                })*/
            //}
        ]);
        CRUD::addColumn([
            'name' => 'validated',
            'label'=> 'Validé',
            'type'=>'closure',
            'function' => function ($entry) {
              
                if (empty($entry->validated)) {
                    return 'non';
                }
                return "oui";
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if (empty($entry->validated)) {
                        return 'badge badge-error';
                    }


                    return 'badge badge-success';
                },
            ],
        ]);
        CRUD::addColumn([
            'name' => 'title',
            'label'=> 'Titre',
            'type'=>'closure',
            'function' => function ($entry) {
              
                if (empty($entry->title)) {
                    return 'non';
                }
                return "oui";
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if (empty($entry->title)) {
                        return 'badge badge-error';
                    }


                    return 'badge badge-success';
                },
            ],
        ]);
        CRUD::addColumn([
            'name' => 'description',
            'label'=> 'Description',
            'type'=>'closure',
            'function' => function ($entry) {
              
                if (empty($entry->description)) {
                    return 'non';
                }
                return "oui";
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if (empty($entry->description)) {
                        return 'badge badge-error';
                    }


                    return 'badge badge-success';
                },
            ],
        ]);
        CRUD::addColumn([
            'name' => 'keywords',
            'label'=> 'Keywords',
            'type'=>'closure',
            'function' => function ($entry) {
              
                if (empty($entry->keywords)) {
                    return 'non';
                }
                return "oui";
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if (empty($entry->keywords)) {
                        return 'badge badge-error';
                    }


                    return 'badge badge-success';
                },
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::field('title');
        CRUD::field('keywords');
        CRUD::field('description');
        CRUD::field('validated');
    }

    protected function retrieveIndexableElement($crud, $column, $entry, $related_key)
    {
        $controller = $this->retrieveRelatedCrudController($entry->indexed);
        if ($controller) {
            return 'a';
        }
        return 'span';
    }

    protected function retrieveIndexableHref($crud, $column, $entry, $related_key)
    {
        $controller = $this->retrieveRelatedCrudController($entry->indexed);
        if ($controller) {
            return backpack_url($controller . '/' . $entry->indexed->id . '/show');
        }
    }

    protected function retrieveRelatedCrudController($item)
    {
        $class = $item ?  get_class($item): '';
        return match ($class) {
       

            default => NULL
        };
    }


    protected function retrieveRelatedClass($crud, $column, $entry, $related_key)
    {

        $class =  $entry->indexed ? get_class($entry->indexed): '';
        return match ($class) {
            default => 'nav-link '
        };
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
