<?php 

namespace KDA\SEO\Admin\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

class SeoRecord extends \KDA\SEO\Models\SeoRecord{


    use CrudTrait;

    public function scopeOnlyValidated($q){
        return $q->where('validated',true);
    }

    public function scopeNotValidated($q){
        return $q->where('validated',false);
    }
}